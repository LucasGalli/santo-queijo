var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

var options = {
	onKeyPress: function (cpf, ev, el, op) {
		var masks = ['000.000.000-000', '00.000.000/0000-00'],
			mask = (cpf.length > 14) ? masks[1] : masks[0];
		el.mask(mask, op);
	}
};

$(document).ready(function() {
	var windowWidth = $(window).width();
	var navbarHeight = $('.nav-custom').height();

	$('[data-toggle="tooltip"]').tooltip();
	$(".fancybox").fancybox({});
	$(".thumbs").simpleGal({
		mainImage: ".selected-image"
	});

	$('.mask-cpf-cnpj').mask('000.000.000-000', options);
	$('.mask-phone').mask(SPMaskBehavior, spOptions);

	$('.nav li a').on('click', function() {
		if(windowWidth < 768) {
			$('.navbar-toggle').click();
		}
	});

	$('a[href*="#"]').on('click', function (e) {
		if(windowWidth < 768) {
			navbarHeight = 0;
		}
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - navbarHeight
		}, 1200, 'swing');
	});

});

$("form").on("submit", function (e) {
	var $button = $(this).find('button[data-loading-text]');

	if ($button.length) {
		$($button).button("loading");
	}
});

var $carousel = $('.main-carousel').flickity({
	cellAlign: 'left',
	contain: true,
	draggable: true,
	wrapAround: true,
	groupCells: false,
	pageDots: false,
	fullscreen: true,
	prevNextButtons: true,
	imagesLoaded: true,
	autoPlay: 3000
});

$(document).on("click",".botao-abrir-carousel", function(e) {
	e.preventDefault();
	$(this).siblings('.main-carousel').find('.flickity-fullscreen-button-view').trigger('click');
	$('#body-js').append($('.carousel-molduras'));
});

$(document).on("click",".carousel-molduras .flickity-fullscreen-button-exit", function(e) {
	e.preventDefault();
	$('.eventos__quadros').append($('.carousel-molduras'));
	$carousel.flickity('resize');
});
