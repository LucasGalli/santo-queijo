<?php
/**
 * A very flexible, full and easy cart solution for FuelPHP
 *
 * @package		Cart
 * @version		1.0
 * @author		Frank de Jonge (FrenkyNet)
 * @license		MIT License
 * @copyright	2010 - 2012 Frank de Jonge
 * @link		http://frankdejonge.nl
 */

/*

Needed database schema:

CREATE TABLE `carts` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `contents` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

*/

namespace Cart;

class Cart_Auth extends \Cart_Driver {

	/**
	 * Get the user's id.
	 *
	 * @return	mixed	the user's id
	 */
	protected function _user_id()
	{
		if(array_key_exists('impersonate', $this->config))
		{
			return $this->config['impersonate'];
		}
		$user_id = \Auth::instance($this->config['auth_instance'])->get_user_id();
		return $user_id[1];
	}

	/**
	 * Returns the datastring.
	 *
	 * @param	string	$key		storage key
	 * @return	string|array		datastring or empty array of not found
	 */
	protected function _get($key)
	{
		$user_id = $this->_user_id();

		if($user_id === 0)
		{
			return \Session::get($key, array());
		}

		$cart = \DB::select()
			->from($this->config_get('cart_table', 'carts'))
			->as_assoc()
			->where('identifier', $key)
			->and_where('user_id', $user_id)
			->execute();

		$cookie_fallback = \Session::get($key, false);

		$return = (count($cart) > 0) ? $cart[0]['contents'] : array();

		if($cookie_fallback !== false)
		{
			\Session::delete($key);

			if(empty($return))
			{
				return $cookie_fallback;
			}

			is_array($return) or $return = unserialize(stripslashes($return));
			$cookie_fallback = unserialize(stripslashes($cookie_fallback));
			$return = array_merge($return, $cookie_fallback);
		}

		return $return;
	}

	/**
	 * Stores the data.
	 *
	 * @param	string	$key			storage key
	 * @param	string	$data_string	serialized data string
	 */
    protected function _set($key, $data_string)
    {
        $user_id = $this->_user_id();

        if($user_id === 0)
        {
            return \Session::set($key, $data_string, $this->config['cookie_expire']);
        }

        $items = @unserialize(stripslashes($data_string));
        $count_item = (is_array($items)) ? count($items) : 0;

        $count = (int) \DB::select(\DB::expr('COUNT(*) as count'))
            ->from($this->config_get('cart_table', 'carts'))
            ->where('identifier', $key)
            ->and_where('user_id', $user_id)
            ->as_object()
            ->execute()
            ->current()
            ->count;

        if ($count_item == 0)
        {
            \DB::delete($this->config_get('cart_table', 'carts'))
                ->where('identifier', '=', $key)
                ->and_where('user_id', $user_id)
                ->execute();
        }
        else
        {
            if($count < 1)
            {
                \DB::insert($this->config_get('cart_table', 'carts'))
                    ->set(array(
                        'identifier' => $key,
                        'user_id' => $this->_user_id(),
                        'contents' => $data_string,
                        'count' => $count_item,
                        'created_at' => \Date::forge()->get_timestamp()
                    ))
                    ->execute();
            }
            else
            {
                \DB::update($this->config_get('cart_table', 'carts'))
                    ->set(
                        array(
                            'contents' => $data_string,
                            'count' => $count_item,
                            'updated_at' => \Date::forge()->get_timestamp()
                        )
                    )
                    ->where('identifier', '=', $key)
                    ->and_where('user_id', $user_id)
                    ->execute();
            }
        }
    }

	/**
	 * Deletes the data.
	 *
	 * @param	string	$key		storage key
	 */
	protected function _delete($key)
	{
		$user_id = $this->_user_id();;

		if($user_id === 0)
		{
			return \Session::delete($key);
		}

		\DB::delete($this->config_get('cart_table', 'carts'))
			->where('identifier', '=', $key)
			->and_where('user_id', $user_id)
			->execute();
	}

}