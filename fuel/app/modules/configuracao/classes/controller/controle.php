<?php

namespace Configuracao;

class Controller_Controle extends \Controller_Controle_Template
{
	/**
	 * Título da página no plural e singular
	 * Está definindo no construct
	 * @var array
	 */
	protected $titulo = array(
		'plural' => 'configurações',
		'singular' => 'configuração',
	);


	public function action_index()
	{
		\Response::redirect(\Uri::controller('editar/1'));

	}

	public function action_adicionar($id = null)
	{
		$data = array();
		$data['titulo'] = $this->titulo;

		if ($id) {
			if (!$registro = Model_Configuracao::find($id)) {
				throw new \HttpNotFoundException;
			}
		} else {
			throw new \HttpNotFoundException;
		}

		if (\Input::method() == 'POST') {

			$val = $registro->validation();

			if ($val->run()) {
				$registro->titulo = \Input::post('titulo');
				$registro->telefone = \Input::post('telefone');
				$registro->celular = \Input::post('celular');
				$registro->email = \Input::post('email');
				$registro->endereco = \Input::post('endereco');

				$registro->facebook = \Input::post('facebook');
				$registro->google_plus = \Input::post('google_plus');
				$registro->instagram = \Input::post('instagram');
				$registro->linkedin = \Input::post('linkedin');
				$registro->pinterest = \Input::post('pinterest');
				$registro->youtube = \Input::post('youtube');
				$registro->twitter = \Input::post('twitter');

				$registro->meta_title = \Input::post('meta_title');
				$registro->meta_description = \Input::post('meta_description');
				$registro->meta_keywords = \Input::post('meta_keywords');

				$registro->conteudo_extra_css = \Security::html_entity_decode(\Input::post('conteudo_extra_css'));
				$registro->conteudo_extra_texto = \Security::html_entity_decode(\Input::post('conteudo_extra_texto'));

				try {
					$registro->save();

					\Message::success(__('message.salvar.sucesso'));
					\Response::redirect(\Uri::controller('editar/' . $registro->id));
				} catch (\Exception $e) {
					\Message::error(__('message.salvar.erro'));
				}
			} else {
				\Message::error($val->error());
			}
		}

		$data['registro'] = $registro;

		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/form', $data);

	}

	public function action_editar($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		$this->action_adicionar($id);
	}
}
