<?php

namespace Contato;
class Controller_Controle extends \Controller_Controle_Template
{
	/**
	 * Título da página no plural e singular
	 * Está definindo no construct
	 * @var array
	 */
	protected $titulo = array(
		'plural' => 'contatos',
		'singular' => 'contato',
	);


	public function action_index()
	{
		\Response::redirect(\Uri::controller('listar'));

	}

	public function action_listar()
	{
		$data = array();
		$data['titulo'] = $this->titulo;

		$query_registros = \Contato\Model_Contato::query();

		if (\Input::method() == 'GET') {
			if (\Input::get('fast_query') != '') {
				$query_registros
					->and_where_open()
					->where('nome', 'like', '%' . \Input::get('fast_query') . '%')
					->or_where('email', 'like', '%' . \Input::get('fast_query') . '%')
					->and_where_close();
			}

			if (\Input::get('fast_tipo') != '') {
				$query_registros->where('tipo', \Input::get('fast_tipo'));
			}
		}

		/* Configuração da paginação particular. */
		$this->pagination['total_items'] = $query_registros->count();

		/* Configuração da ordenação */
		$this->tablesort['columns'] = array(
			array('<input type="checkbox" class="check_all" />', '', array('width' => '5%', 'class' => 'text-center')),
			array('Tipo', 'tipo', array('width' => '5%')),
			array('Nome', 'nome', array('width' => '40%')),
			array('E-mail', 'email', array('width' => '40%')),
			array('Data', 'created_at', array('width' => '10%')),
		);

		\Pagination::set_config($this->pagination);
		\TableSort::set_config($this->tablesort);

		$data['registros'] = $query_registros->order_by(\TableSort::get('sort_by'), \TableSort::get('direction'))->rows_limit(\Pagination::get('per_page'))->rows_offset(\Pagination::get('offset'))->get();

		if (\Input::is_ajax() === true) {
			$view = 'listar_registros';
		} else {
			$view = 'listar';
		}

		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/' . $view, $data);
	}

	public function action_visualizar($id = null)
	{
		$data = array();

		if ($id) {
			if (\Auth::has_access('contato.visualizar') and !$registro = \Contato\Model_Contato::find($id)) {
				throw new \HttpNotFoundException;
			}
		}

		$data['registro'] = $registro;

		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/visualizar', $data);
	}

	public function action_exportar()
	{
		if (\Auth::has_access('contato.exportar') and $this->exportar()) {
			\Message::success(__('message.exportar.sucesso'));
		} else {
			\Message::error(__('message.exportar.erro'));
		}

		\Response::redirect_back(\Uri::controller('listar'));
	}

	public function exportar()
	{

		$data = array();
		$contatos = \Contato\Model_Contato::query()->get();

		$excel = \PHPExcel_IOFactory::load(DOCROOT . 'assets' . DS . 'controle' . DS . 'excel' . DS . "contatos.xlsx");
		$excel->setActiveSheetIndex(0);
		$writer = \PHPExcel_IOFactory::createWriter($excel, "Excel2007");
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$excel->getActiveSheet()->setCellValue('A1', 'Data: ' . \Date::forge()->format('br'));

		/* Homologações */
		$linha = 4;
		foreach ($contatos as $contato) {
			$excel->getActiveSheet()->setCellValue('A' . $linha, $contato->nome);
			$excel->getActiveSheet()->setCellValue('B' . $linha, $contato->email);
			$excel->getActiveSheet()->setCellValue('C' . $linha, $contato->telefone);
			$excel->getActiveSheet()->setCellValue('D' . $linha, $contato->mensagem);
			$excel->getActiveSheet()->setCellValue('E' . $linha, $contato->created_at('br_full'));

			$excel->getActiveSheet()->getStyle('A' . $linha . ':E' . $linha)->applyFromArray($styleArray);
			$linha++;
		}

		$writer->save(DOCROOT . 'assets' . DS . 'upload' . DS . 'contatos' . '.xlsx');
		\File::download(DOCROOT . 'assets' . DS . 'upload' . DS . 'contatos' . '.xlsx', 'Contatos' . '.xlsx', null, null, true);

	}


	public function action_excluir($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		if ($this->excluir($id)) {
			\Message::success(__('message.excluir.sucesso'));
		} else {
			\Message::error(__('message.excluir.erro'));
		}

		\Response::redirect_back(\Uri::controller('listar'));
	}

	public function action_aplicar()
	{
		$this->aplicar();

	}

	public function excluir($id = null)
	{
		if (is_numeric($id)) {
			if (\Auth::has_access('contato.excluir') and $registro = \Contato\Model_Contato::find($id) and $registro->delete()) {
				return true;
			}
		}
		return false;
	}
}
