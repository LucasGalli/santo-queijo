<?php

namespace Contato;

class Model_Contato extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'tipo',
        'nome',
        'email',
        'assunto',
        'mensagem',
        'arquivo',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array(
        '\Observer_Controle' => array(
            'events' => array('before_delete'),
        ),
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_save'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'contato';

	protected static $_tipo = array(
		'0' => array('titulo' => 'Contato', 'class' => 'default'),
		'1' => array('titulo' => 'Clube Santo Queijo', 'class' => 'info'),
	);

	public static function lista_tipos()
	{
		$tipos = array();
		$tipos[''] = 'Todos';
		foreach (static::$_tipo as $indice => $valor) {
			$tipos[$indice] = $valor['titulo'];
		}
		return $tipos;
	}

	public function tipo($formato = 'titulo') {
		return static::$_tipo[$this->tipo][$formato];
	}

    public function arquivo()
    {
        if (isset($this->arquivo) and $this->arquivo) {
            $retorno = '<i class="fa fa-file-text-o"></i> ';
            $retorno .= ($this->nome) ?: $this->arquivo;

            return \Html::anchor('/assets/upload/' . $this->arquivo, $retorno, array('title' => $this->nome, 'target' => '_blank'));
        }

        return null;

    }

    public function created_at($format = 'br')
    {
        if ($this->created_at) {
            return \Date::forge($this->created_at)->format($format);
        }
        return false;
    }

    public function validation()
    {
        $val = \Validation::forge();
        \Lang::load('form', true);

		if($this->tipo == 0){
			$val->add_field('nome', __('form.nome.label'), 'required|min_length[3]|max_length[150]');
			$val->add_field('email', __('form.email.label'), 'required|min_length[3]|max_length[200]|valid_email');
			$val->add_field('assunto', __('form.assunto.label'), 'required|max_length[150]');
			$val->add_field('mensagem', __('form.mensagem.label'), 'required|min_length[5]');
		}else{
			$val->add_field('email', __('form.email.label'), 'required|min_length[3]|max_length[200]|valid_email');
		}

        return $val;
    }
}
