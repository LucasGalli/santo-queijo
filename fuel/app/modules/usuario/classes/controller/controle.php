<?php

namespace Usuario;

class Controller_Controle extends \Controller_Controle_Template
{
	/**
	 * Título da página no plural e singular
	 * @var array
	 */
	protected $titulo = array(
		'plural' => 'usuários',
		'singular' => 'usuário'
	);

	public function action_index()
	{
		\Response::redirect(\Uri::controller('/listar'));
	}

	public function action_listar()
	{
		$data = array();
		$data['titulo'] = $this->titulo;

		$query_registros = Model_Usuario::query()->where('id', '<>', $this->current_user->id);

		if (\Input::method() == 'GET') {
			if (\Input::get('fast_query') != '') {
				$query_registros->and_where_open()
					->where('nome', 'like', '%' . \Input::get('fast_query') . '%')
					->or_where('username', 'like', '%' . \Input::get('fast_query') . '%')
					->or_where('email', 'like', '%' . \Input::get('fast_query') . '%')
					->and_where_close();
			}
		}

		switch (\Input::get('fast_order', 0)) {
			case 0:
				$query_registros->order_by('created_at', 'desc');
				break;
			case 1:
				$query_registros->order_by('nome', 'asc');
				break;
		}

		/* Configuração da paginação particular. */
		$this->pagination['total_items'] = $query_registros->count();
		\Pagination::set_config($this->pagination);

		$data['registros'] = $query_registros->rows_limit(\Pagination::get('per_page'))->rows_offset(\Pagination::get('offset'))->get();
		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/listar', $data);
	}

	public function action_adicionar($id = null)
	{
		$data = array();
		$data['titulo'] = $this->titulo;

		if ($id) {
			if (!$registro = Model_Usuario::find($id) or
				$this->verifica_permissao($registro) == false
			) {
				throw new \HttpNotFoundException;
			}
		} else {
			$registro = Model_Usuario::forge();
			$registro->group = 2;
		}

		if ('POST' == \Input::method()) {
			$val = $registro->validation();
			$registro->group = \Input::post('group');
			$registro->nome = \Input::post('nome');
			$registro->email = \Input::post('email');

			if (!$registro->username) {
				$registro->username = \Input::post('username');
			}

			$registro->status = \Input::post('status');
			$registro->user_id = $this->current_user->id;

			if ($val->run()) {
				$auth = \Auth::instance();

				try {
					$registro->save();

					if (\Input::post('password', '') != '') {
						$old_password = $auth->reset_password($registro->username);
						$auth->change_password($old_password, \Input::post('password'), $registro->username);
					}

					\Message::success(__('message.salvar.sucesso'));
					\Response::redirect(\Uri::controller('editar/' . $registro->id));
				} catch (\Exception $e) {
					\Message::error(__('message.salvar.erro'));
				}
			} else {
				\Message::error($val->error());
			}
		}
		$data['registro'] = $registro;
		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/form', $data);
	}

	public function action_minhaconta()
	{
		$data = array();
		$data['titulo'] = array('singular' => 'minha conta');

		$registro = Model_Usuario::find($this->current_user->id);

		if ('POST' == \Input::method()) {
			$val = $registro->validation();

			$registro->nome = \Input::post('nome');
			$registro->email = \Input::post('email');

			if ($val->run()) {
				$auth = \Auth::instance();

				try {
					$registro->save();
					if (\Input::post('password', '') != '') {
						$old_password = $auth->reset_password($registro->username);
						$auth->change_password($old_password, \Input::post('password'), $registro->username);
					}
					\Message::success(__('message.login.erro'));
				} catch (\Exception $e) {
					\Message::error(__('message.salvar.erro'));
				}
			} else {
				\Message::error($val->error());
			}
		}
		$data['registro'] = $registro;
		$this->template->content = \View::forge(__NAMESPACE__ . '::controle/form', $data);
	}

	public function action_editar($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		$this->action_adicionar($id);
	}

	public function action_inativar($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		if ($this->alterar_status($id, 0)) {
			\Message::success(__('message.inativar.sucesso'));
		} else {
			\Message::error(__('message.inativar.erro'));
		}

		\Response::redirect_back(\Uri::controller('listar'));

	}

	public function action_ativar($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		if ($this->alterar_status($id, 1)) {
			\Message::success(__('message.ativar.sucesso'));
		} else {
			\Message::error(__('message.ativar.erro'));
		}

		\Response::redirect_back(\Uri::controller('listar'));

	}

	public function action_excluir($fk_empresa, $id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		if ($this->excluir($id)) {
			\Message::success(__('message.excluir.sucesso'));
		} else {
			\Message::error(__('message.excluir.erro'));
		}

		\Response::redirect_back(\Uri::controller('listar'));
	}

	public function action_aplicar()
	{
		if ('POST' == \Input::method() and \Input::post('selected_itens')) {
			$registros = \Input::post('selected_itens');
			if (is_string($registros)) {
				$registros = explode(',', $registros);
				array_pop($registros);
			}
			$qtde_error = 0;
			switch (\Input::post('action')) {
				case 'excluir':
					foreach ($registros as $id) {
						if (!$this->excluir($id)) {
							$qtde_error++;
						}
					}
					break;

				case 'inativar':
					foreach ($registros as $id) {
						if (!$this->alterar_status($id, 0)) {
							$qtde_error++;
						}
					}
					break;

				case 'ativar':
					foreach ($registros as $id) {
						if (!$this->alterar_status($id, 1)) {
							$qtde_error++;
						}
					}
					break;
			}

			if ($qtde_error == 0) {
				\Message::success(__('message.aplicar.' . \Input::post('action') . '.sucesso'));
			} else {
				\Message::error(__('message.aplicar.' . \Input::post('action') . '.erro', array('qtde' => $qtde_error)));
			}
		}
		\Response::redirect_back('/controle/usuario/listar');
	}

	protected function excluir($id = null)
	{
		if (is_null($id)) {
			throw new \HttpNotFoundException;
		}

		if (is_numeric($id)) {

			if ($registro = Model_Usuario::find($id) and
				$this->verifica_permissao($registro) and
				$registro->delete()) {
				return true;
			}
		}
		return false;

	}

	public function alterar_status($id = null, $valor = null)
	{
		if (is_numeric($id) and is_numeric($valor)) {
			if ($registro = Model_Usuario::find($id) and
				$this->verifica_permissao($registro)) {
				$registro->status = $valor;

				try {
					$registro->save();
					return true;
				} catch (\Exception $e) {
					return false;
				}
			}
		}

		return false;
	}

	protected function verifica_permissao($registro)
	{
		if ($this->current_user->id != $registro->id) {
			return true;
		} else {
			return false;
		}
	}

	public function action_logout()
	{
		\Auth::logout() or \Response::redirect('/controle/login');
	}
}
