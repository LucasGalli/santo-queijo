<header class="header" id="topo">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 header__title">
				<div class="main-carousel">
					<div class="carousel-cell"><?php echo Asset::img('banner1.png'); ?></div>
					<div class="carousel-cell"><?php echo Asset::img('banner2.png'); ?></div>
					<div class="carousel-cell"><?php echo Asset::img('banner3.png'); ?></div>
					<div class="carousel-cell"><?php echo Asset::img('banner4.png'); ?></div>
				</div>
				<h6>Segunda a sexta, das 8 às 19 horas | Sábado das 9 às 14 horas</h6>
				<div class="header__title__button">
					<?php echo Html::anchor('index.php', '<img src="assets/img/logo.png" class="logo img-responsive">'); ?>
					<?php echo Html::anchor('index.php', '<img src="assets/img/logo2.png" class="logo2 img-responsive">'); ?>
				</div>
				<h1><strong>Da fazenda pra sua mesa!</strong></h1>
			</div>
		</div>
	</div>
</header>
<section class="nossa-historia" id="nossa-historia">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-md-5 col-md-offset-5 nossa-historia__text">
					<h1>nossa história</h1>
					<p> Quem não gosta de queijin, sô?<br>
						Tudo começou no ano de 2006, quando o mineiro Vinícius começou a
						vender seus queijos artesanais no interior do Rio de Janeiro e de São Paulo.<br>
						Naquela época, seu sonho era criar uma empresa capaz de oferecer produtos
						de qualidade. Em 2012, fundou a Santo Queijo na capital Paulista,
						e desde então utiliza de sua experiência e carisma para atrair e criar uma relação de confiança com seus clientes e parceiros.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="eventos" id="eventos">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-md-5 col-md-offset-1 eventos__text">
					<h1>eventos</h1>
					<p> Há 4 anos, a Santo Queijo expandiu o seu negócio. Atendendo em eventos empresariais e residenciais, preparamos o melhor menu degustação para os
						seus convidados aproveitarem o melhor da ocasião!<br><br>
						<b>Faça seu evento e celebre suas conquistas com a gente!</b><br><br>
					</p>
					<?php echo Html::anchor('#contato', 'entre em contato', array('class' => 'btn-contato')); ?>
				</div>
				<div class="col-md-5 col-md-offset-1">
					<div class="eventos__quadros">
						<?php echo Asset::img('moldura.png', array('class' => 'img-responsive moldura')); ?>
						<?php echo Asset::img('moldura2.png', array('class' => 'img-responsive moldura2')); ?>

						<button class="btn botao-abrir-carousel">
							<i class="fa fa-arrows-alt"></i>
						</button>


						<div class="main-carousel carousel-molduras">
							<div class="carousel-cell">
								<?php echo Asset::img('foto1.png', array('class' => 'img-responsive')); ?>
							</div>
							<div class="carousel-cell">
								<?php echo Asset::img('foto2.png', array('class' => 'img-responsive')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="nossos-produtos" >
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-md-6 col-md-offset-3">
					<div class="nossos-produtos__text nossos-produtos__text-drive">
						<h1 id="nossos-produtos text-center">conheça nossos produtos</h1>
						<p> Produzidos na Serra da Canastra, nossos queijos são artesanais e livre de conservantes.<br>
							Dispomos de grande variedade: queijos frescos, meia cura, curados, com e sem recheios.<br>
							Também trabalhamos com doces, embutidos, cachaças, café e geleias.<br><br>
							Espia só:
						</p>
					</div>
					<div class="main-carousel">
						<div class="carousel-cell"><?php echo Asset::img('geleia-de-goiaba.png'); ?> <span class="legenda">geléia de goiaba</span></div>
						<div class="carousel-cell"><?php echo Asset::img('gorgonzola.png'); ?> <span class="legenda">gorgonzola</span></div>
						<div class="carousel-cell"><?php echo Asset::img('linguica.png'); ?> <span class="legenda">linguiça cuiabana</span></div>
						<div class="carousel-cell"><?php echo Asset::img('maasdam.png'); ?> <span class="legenda">maasdam</span></div>
						<div class="carousel-cell"><?php echo Asset::img('cafe.png'); ?> <span class="legenda">café especial torra média</span></div>
						<div class="carousel-cell"><?php echo Asset::img('gouda.png'); ?> <span class="legenda">gouda</span></div>
						<div class="carousel-cell"><?php echo Asset::img('salame.png'); ?> <span class="legenda">salame especial</span></div>
						<div class="carousel-cell"><?php echo Asset::img('geleia-de-morango.png'); ?> <span class="legenda">geléia de morango</span></div>
						<div class="carousel-cell"><?php echo Asset::img('cachaca.png'); ?> <span class="legenda">cachaça selecta</span></div>
					</div>
					<ul class="nossos-produtos__produtos">
						<li class="produto"><?php echo Asset::img('geleia-de-goiaba.png'); ?> <span class="legenda">geléia de<br>goiaba</span></li>
						<li class="produto"><?php echo Asset::img('gorgonzola.png'); ?> <span class="legenda">gorgonzola</span></li>
						<li class="produto"><?php echo Asset::img('linguica.png'); ?> <span class="legenda">linguiça<br>cuiabana</span></li>
						<li class="produto"><?php echo Asset::img('maasdam.png'); ?> <span class="legenda">maasdam</span></li>
						<li class="produto"><?php echo Asset::img('cafe.png'); ?> <span class="legenda">café especial<br>torra média</span></li>
						<li class="produto"><?php echo Asset::img('gouda.png'); ?> <span class="legenda">gouda</span></li>
						<li class="produto"><?php echo Asset::img('salame.png'); ?> <span class="legenda">salame<br>especial</span></li>
						<li class="produto"><?php echo Asset::img('geleia-de-morango.png'); ?> <span class="legenda">geléia de<br>morango</span></li>
						<li class="produto"><?php echo Asset::img('cachaca.png'); ?> <span class="legenda">cachaça<br>selecta</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="clube" id="clube">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<?php echo Asset::img('cesta.png'); ?>
				<div class="col-md-5 col-md-offset-4 clube__text">
					<h1>clube santo queijo</h1>
					<p> Assine de forma rápida e receba mensalmente uma seleção primorosa de
						nossos produtos em sua casa!
					</p>
					<form action="#form-clube-santo-queijo" method="POST" class="form" enctype="multipart/form-data" id="form-clube-santo-queijo">
						<?php
						if (\Input::post('tipo') == 1):
							echo \Message::get();
						endif;
						?>
						<div class="alert" style="display: none;"></div>
						<?php echo Form::hidden('tipo', 1); ?>
						<div class="form-group">
							<?php echo Form::input('email', (Input::post('tipo') == 1 ? Input::post('email') : ''), array('class' => 'form-control input-clube', 'placeholder' => 'e-mail')); ?>
						</div>
						<?php
						echo Form::button('enviar', 'assinar', array('value' => 'submit', 'class' => 'form-control btn-clube'));
						echo Form::close();
						?>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="contato" id="contato">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-md-6 contato__text">
					<h1>contato</h1>
					<p> Entre em contato conosco!<br>
						Faça uma reserva para seu evento ou nos visite para provar<br>
						nossos deliciosos produtos.
					</p>
					<?php
					if (\Input::post('tipo') == 0):
						echo \Message::get();
					endif;
					?>
					<div class="alert" style="display: none;"></div>
					<?php echo Form::hidden('tipo', 0); ?>
					<?php
					echo \Message::get();
					?>
					<form action="#contato"  method="POST" class="form" enctype="multipart/form-data" onsubmit="document.form.reset()">
						<div class="form-group">
							<?php echo Form::input('nome', (Input::post('tipo') == 0 ? Input::post('nome') : ''), array('class' => 'form-control nome-contato', 'placeholder' => 'nome', 'required')); ?>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::input('email', (Input::post('tipo') == 0 ? Input::post('email') : ''), array('class' => 'form-control email-contato','type' => 'email', 'placeholder' => 'e-mail', 'required')); ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php echo Form::input('assunto', (Input::post('tipo') == 0 ? Input::post('assunto') : ''), array('class' => 'form-control assunto-contato', 'placeholder' => 'assunto', 'required')); ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo Form::textarea('mensagem', (Input::post('tipo') == 0 ? Input::post('mensagem') : ''), array('class' => 'form-control mensagem-contato', 'placeholder' => 'mensagem', 'rows' => 5, 'cols' => 8)); ?>
						</div>
						<div class="col-md-6">
							<div name="g-recaptcha-response" class="g-recaptcha g-recaptcha-response" data-sitekey="<?php echo \Config::get('site.recaptcha.sitekey'); ?>"></div>
						</div>
						<div class="col-md-6">
							<?php
							echo Form::button('enviar', 'enviar', array('value' => 'submit', 'class' => 'form-control btn-contato', 'data-loading-text'=> '<i class="fa fa-spinner fa-spin"></i>'));
							echo Form::close();
							?>
						</div>
					</form>
				</div>
				<div class="col-md-5 contato__localizacao">
					<h1>localização</h1>
					<div class="gmap_canvas">
						<iframe id="gmap_canvas" src="https://maps.google.com/maps?q=R.%20S%C3%A3o%20Tom%C3%A9%2C%20119%2C%20Sala%20n%C2%BA%2091%20-%20Vila%20Ol%C3%ADmpia%20&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					</div>
					<div class="contato__localizacao__text">
						<p>
							R. São Tomé, 119, Sala nº 91 - Vila Olímpia<br>
							São Paulo - SP, Brasil<br>
							<b>(11) 3849-5656<br><br>
								comercial | eventos | atendimento</b>@santoqueijo.com.br
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="footer">
	<footer class="page-footer">
		<div class="footer-copyright text-left">
			<div class="container">
				<div class="col-md-5 col-md-offset-1">
					<p>Segunda a sexta, das 8 às 19 horas |<br>Sábado das 9 às 14 horas</p>
				</div>
				<div class="col-md-5 col-md-offset-1">
					<p class="direitos"><b>Santo Queijo</b> - © 2018 - Todos os direitos reservados.</p>
				</div>
			</div>
		</div>
	</footer>
</section>
