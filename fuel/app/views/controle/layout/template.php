<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title><?php echo e($title); ?></title>
	<meta charset="utf-8" />
	<meta name="robots" content="noindex,nofollow">
	<meta name="author" content="Revelare, Agência de Comunicação + Tecnologia" />
    <link id="page_favicon" href="<?php echo Uri::create('favicon.png'); ?>" rel="icon" type="image/x-icon"/>
	<?php
		if (Auth::check())
		{
	?>
	<script type="text/javascript">
		var BASE = '<?php echo Uri::base(false); ?>';
		var DOMAIN = '<?php echo Config::get("site.dominio"); ?>';
		var CKEDITOR_BASEPATH = '<?php echo Uri::create("assets/controle/lib/ckeditor/"); ?>';
		var UPLOAD_URL = '';
	</script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<?php
		}
		echo Casset::render_css();
		echo Casset::render_css_inline();
	?>
	<script type="text/javascript" src="<?php echo Uri::create('assets/controle/js/jquery.min.js'); ?>"></script>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body class="<?php echo isset($body_class) ? $body_class : null; ?>">
	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="header-info">
						<div class="partner-logo">
						<?php echo Html::anchor(Uri::create('/controle'), \Casset::img('logo.png', Config::get('site.titulo'))); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?php
		if ($current_user)
		{
	?>
	<div class="navbar navbar-st navbar-static-top" role="navigation">
		<div class="container">
			<?php echo Menu::instance('menu_controle')->render(); ?>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<small><span class="glyphicon glyphicon-user"></span></small> <?php echo $current_user->nome; ?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo Uri::create('controle/minhaconta'); ?>">Minha Conta</a></li>
						<li><a href="<?php echo Uri::create('controle/logout'); ?>">Sair</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<?php
		}
	?>

	<div class="content">
		<div class="container">
			<?php
			if ($current_user)
			{
				echo \Breadcrumb_Controle::create_links();
			}
			?>

			<div class="row">
					<?php
						$main_span = 12;
						if (isset($sidebar))
						{
							$main_span = 9;
					?>
							<div class="col-md-3">
								<div class="sidebar well well-sm" role="complementary">
									<?php echo \Security::html_entity_decode($sidebar); ?>
								</div>
							</div>
					<?php
						}
					?>
					<div class="col-md-<?php echo $main_span; ?>" role="main">
						<?php
						if (isset($topbar)) {
							echo $topbar;
						}

						echo \Message::get();
						echo $content;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="row">
                <?php $empresa = Config::get('site.empresa');?>
                <?php $cliente_de = Config::get('site.cliente_de'); ?>
				<div class="col-md-9">
					&copy; <?php echo Date::forge()->format("%Y"); ?> <?php echo \Config::get('site.titulo'); ?>. Todos os direitos são reservados.
					<br />
					Dúvidas? Entre em contato através do<?php echo (count($cliente_de[$empresa]['telefones']) > 1) ? 's' : '' ?>
                    telefone<?php echo (count($cliente_de[$empresa]['telefones']) > 1) ? 's' : '' ?>
					<?php if (count($cliente_de[$empresa]['telefones']) > 1) { ?>
						<?php foreach ($cliente_de[$empresa]['telefones'] as $telefone) { ?>
							<?php echo $telefone; ?>
						<?php } ?>
					<?php } else { ?>
						<?php echo $cliente_de[$empresa]['telefones'][0]; ?>
					<?php } ?>
				</div>
				<div class="col-md-3 text-right">
					<?php echo  \Casset::img($cliente_de[$empresa]['logo'], $cliente_de[$empresa]['titulo']); ?>
				</div>
			</div>
		</div>
	</footer>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaxxJfKX1pb5ZyIaG6KlpVze8vbD0nmcQ" type="text/javascript"></script>
	<?php
		echo Casset::render_js();
		echo Casset::render_js_inline();
	?>
</body>
</html>
