<?php
class Pagination extends Fuel\Core\Pagination
{
	/**
	 * Prepares vars for creating links
	 */
	protected function _recalculate()
	{
		// calculate the number of pages
		$this->config['total_pages'] = (int) ceil($this->config['total_items'] / $this->config['per_page']) ?: 1;

		// get the current page number, either from the one set, or from the URI or the query string
		if ($this->config['current_page'])
		{
				$this->config['calculated_page'] = $this->config['current_page'];
		}
		else
		{
			if (is_string($this->config['uri_segment']))
			{
				$this->config['calculated_page'] = \Input::get($this->config['uri_segment'], 1);
			}
			else
			{
				$this->config['calculated_page'] = (int) \Request::main()->uri->get_segment($this->config['uri_segment']);
			}
		}

		// make sure the current page is within bounds
		if ($this->config['calculated_page'] < 1)
		{
			$this->config['calculated_page'] = 1;
		}

		// the current page must be zero based so that the offset for page 1 is 0.
		$this->config['offset'] = ($this->config['calculated_page'] - 1) * $this->config['per_page'];
	}
}
