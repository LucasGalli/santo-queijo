<?php

class Uri extends \Fuel\Core\Uri
{

    public function __construct($uri = NULL)
    {
        parent::__construct($uri);
        $this->detect_language();
    }

    public function detect_language()
    {
        if (!count($this->segments)) {
            return false;
        }

        $first = $this->segments[0];
        $locales = Config::get('locales');

        if (array_key_exists($first, $locales)) {
            array_shift($this->segments);
            $this->uri = implode('/', $this->segments);

            Config::set('language', $first);
            Config::set('locale', $locales[$first]);
        }
    }

    public static function url($uri = '', $params = array())
    {
        $lang = Config::get('language');

        if (!empty($uri)) {
            $lang .= '/';
        }

        if ($new_uri = Router::get($uri, $params)) {
            return substr_replace($new_uri, $lang, strlen(\Config::get('base_url')), 0);
        }

        return Uri::create($lang . $uri);
    }

    /**
     * Retorna o link do controller, sem methods/params
     * @param @href
     * @return string
     */
    public static function controller($href = '')
    {
        $url = static::main();
        $action = \Request::active()->action;

        if (($pos = strpos($url, $action)) !== false) {
            $url = substr($url, 0, $pos);
        }

        if ($href != '') {
            $url = rtrim($url, '/') . '/' . ltrim($href, '/');
        }

        return $url;
    }

    /**
     * Gera o link do botão voltar
     * @param  boolean $gera_url [description]
     * @return [type]            [description]
     */
    public static function back()
    {
        $url = \Uri::main();

        if ((strpos($url, '/controle/') !== false and strpos($url, '/editar/') !== false and strpos(Input::referrer(), '/adicionar') !== false) or Input::referrer() == $url) {
            return Session::get('http_referrer', 'javascript:history.back(-1);');
        }

        if ($referrer = \Input::referrer(Session::get('http_referrer'))) {
            if (strpos($referrer, \Uri::base()) === 0 and $referrer != \Uri::main()) {
                return $referrer;
            }
        }
        return 'javascript:history.back(-1);';
    }


    /**
     * Seta a url anterior na SESSION se não existir o HTTP REFERER
     * @return none
     */
    public static function set_back()
    {
        $url_anterior = \Uri::main();

        if (!(strpos($url_anterior, '/controle/') !== false and strpos($url_anterior, '/editar/') !== false and strpos(Input::referrer(), '/adicionar') !== false)) {
            if ($referrer = \Input::referrer()) {
                if (strpos($referrer, \Uri::base()) === 0 and $referrer != \Uri::main()) {
                    Session::set('http_referrer', $referrer);
                }
            }
        }
    }
}
