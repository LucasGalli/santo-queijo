<?php

class Controller_Frontend_Template extends Controller_Template
{
    public $template = 'frontend/template';
    public $paginas = null;

    protected $pagination = array(
        'pagination_url' => null,
        'total_itens' => 0,
        'per_page' => 3,
        'num_links' => 5,
        'uri_segment' => 'pag',
    );


    public function before()
    {
        if (!defined('LANGUAGE')) define('LANGUAGE', \Lang::get_lang());

        parent::before();
    }

    public function after($r)
    {
        $r = parent::after($r);

        /* Seta a URL Anterior */
        \Uri::set_back();

        /*
        * SEO
        */
        $title = \Config::get('site.geral.titulo');

        if (!isset($this->template->seo) or !array_key_exists('title', $this->template->seo) or $this->template->seo['title'] == '') {
            $this->template->title = $title;
        } else {
            $this->template->title = $this->template->seo['title'] . ' - ' . $title;
        }

        /**
         * Se for AJAX, abre sem template
         */
        if (\Input::is_ajax() === true and is_null($r)) {
            return \Response::forge($this->template->conteudo);
        }

		$this->template->conteudo_extra_css = \Security::html_entity_decode(\Config::get('site.geral.conteudo_extra_css'));
		$this->template->conteudo_extra_texto = \Security::html_entity_decode(\Config::get('site.geral.conteudo_extra_texto'));

        return $r;
    }

    public function action_404()
    {
        $this->template->title = 'Página não encontrada';
        $this->template->meta_description = '';
        $this->template->conteudo = View::forge('errors/404');
    }
}
