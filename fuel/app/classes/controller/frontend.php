<?php

class Controller_Frontend extends Controller_Frontend_Template
{
    public function action_index()
    {
        $data = array();

        if (Input::post())
		{
			$val = Validation::forge('my_validation');
			$val->add_field('nome', 'nome', 'required');
			$val->add_field('email', 'email', 'required|valid_email');
			$val->add_field('assunto', 'assunto', 'required');
			$val->add_field('mensagem', 'mensagem', 'required');

			if ($val->run())
			{

				try {
					/* salvando no DB */
					$mail = new \Contato\Model_Contato();
					$mail->set(\Input::post());
					$mail->save();
				} catch (\Expection $e) {
				}

				$mail = \Email::forge();
				$mail->to(\Config::get('site.geral.email', 'webmaster@revelare.com.br'));
				$mail->reply_to(\Input::post('email'));
				$mail->subject('['.\Config::get('site.geral.titulo').'] Contato do Site');
				$mail->html_body(\View::forge('email/template', array('titulo' => '['.\Config::get('site.geral.titulo').'] Contato do Site'))
					->set('texto', \View::forge('email/contato', \Input::post()), false));

				try {

					if (\Fuel::$env == \Fuel::PRODUCTION)
					{
						$mail->send();
					}
					Message::success('Sua mensagem foi enviada com sucesso! Em breve responderemos seu contato.');
				}
				catch (\EmailSendingFailedException $e)
				{
					\Message::error('Desculpe. Não foi possível enviar seu contato.');
				}
				catch (\Exception $e)
				{
					\Message::error('Desculpe. Não foi possível enviar seu contato.');
				}
				/*Enviando ao agendor*/
				try {

					$url='https://api.agendor.com.br/v3/people/';
					$curl = curl_init($url);
					$curl_post_data = array(
						'name' =>  \Input::post('nome'),
						'contact[email]'=> \Input::post('email'),
						'role' => 'Contato vindo do site do Santo Queijo. Assunto: '.\Input::post('assunto').'.',
						'description' => \Input::post('mensagem'),
					);
					curl_setopt($curl,	 CURLOPT_HTTPHEADER, array(
						'Authorization: token b632bce1-fb70-49e0-97d7-3534850e0e61',
					));
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, 1);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					curl_exec($curl);
					curl_close($curl);
				}
					catch(\Exception $e){
						\Message::error('Desculpe. Não foi possível enviar sua mensagem.');
					}

			}
			else
			{
				Message::error($val->error());
			}
		}

        $this->template->conteudo = \View::forge('frontend/index', $data, false);
    }
}
