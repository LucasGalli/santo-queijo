<?php

class Controller_Frontend_Auth extends Controller_Frontend_Template
{
	public function action_login()
	{
		if (Auth::check()) {
			Response::redirect_back();
		}

		$data = array();

		if (Input::post()) {
			$val = Validation::forge();
			$val->add('username', 'Usuário')->add_rule('required');
			$val->add('password', 'Senha')->add_rule('required');

			if ($val->run()) {
				if (\Auth::login(\Input::post('username'), \Input::post('password'))) {
					$id = Auth::get('id');
					$usuario = \Usuario\Model_Usuario::find($id);

					/* gravo o último login na empresa */
					$usuario->last_login = \Date::forge()->get_timestamp();
					$usuario->save();
					\Auth::remember_me();

					\Response::redirect('area-restrita');
				} else {
					Message::error('Os dados de acesso estão incorretos.');
				}
			} else {
				Message::error($val->error());
			}
		}

		$this->template->conteudo = \View::forge('frontend/auth/login', $data, false);
	}

	public function action_recuperar_senha()
	{
		if (Auth::check()) {
			Response::redirect_back();
		}

		$data = array();
		$view = 'frontend/auth/recuperarsenha/recuperar_senha';

		if (Input::post()) {
			$val = \Usuario\Model_Usuario::forge()->validation('recuperarsenha');

			if ($val->run()) {
				$registro = \Usuario\Model_Usuario::query()
					->where('email', \Input::post('email'))
					->where('status', 1)
					->get_one();
				if ($registro) {
					if ($registro->enviar_recuperar_senha()) {
						$data['registro'] = $registro;
						$view = 'frontend/auth/recuperarsenha/enviado';
					} else {
						\Message::error(__('message.email.erro'));
					}
				} else {
					\Message::error(__('message.recuperarsenha.nao_encontrado'));
				}
			} else {
				\Message::error($val->error());
			}
		}

		$this->template->conteudo = \View::forge($view, $data, false);
	}

	public function action_alterar_senha($hash = null)
	{
		if (\Auth::check() or !isset($hash)) {
			Response::redirect_back();
		}
		$data = array();
		$view = 'frontend/auth/recuperarsenha/alterar_senha';

		$recuperar_senha_hash = substr($hash, 0, 32);
		$id = \Crypt::decode(substr($hash, 32, strlen($hash)));

		if (is_numeric($id) and $id > 0 and strlen($recuperar_senha_hash) == 32) {

			$registro = \Usuario\Model_Usuario::query()->where('id', $id)
				->where('recuperar_senha_hash', $recuperar_senha_hash)
				->where('status', 1)
				->get_one();

			if ($registro) {
				$data['registro'] = $registro;

				if (\Input::method() == 'POST') {
					$val = $registro->validation('recuperarsenha_alterarsenha');

					if ($val->run()) {
						$auth = \Auth::instance();

						$old_password = $auth->reset_password($registro->username);
						if ($auth->change_password($old_password, \Input::post('password'), $registro->username)) {

							try {
								$registro->recuperar_senha_hash = '';
								$registro->save();

								\Message::success(__('message.recuperarsenha.sucesso'));
								\Response::redirect('/login');
							} catch (\Exception $e) {
								/* Não exibe nada, pois o usuário já alterou a senha. */
							}
						} else {
							\Message::error('message.recuperarsenha.erro');
						}
					} else {
						\Message::error($val->error());
					}
				}
			} else {
				$view = 'frontend/auth/recuperarsenha/invalido';
			}
		} else {
			$view = 'frontend/auth/recuperarsenha/invalido';
		}

		$this->template->conteudo = \View::forge($view, $data, false);
	}

	public function action_area_restrita()
	{
		if (!Auth::check()) {
			Response::redirect_back();
		}

		$data = array();
		$data['arquivos'] = \Arquivo\Model_Arquivo::query()->where('fk_usuario', $this->usuario->id)->order_by('titulo', 'ASC')->get();

		$this->template->conteudo = \View::forge('frontend/auth/area_restrita', $data, false);
	}

	public function action_logout()
	{
		\Auth::logout();
		Response::redirect('/');
	}
}
