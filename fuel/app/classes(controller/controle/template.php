<?php

class Controller_Controle_Template extends Controller_Template
{
	public $template = 'controle/layout/template';

	public $permission = null;
	public $controller = null;
	public $action = null;

	protected $pagination = array(
		'pagination_url' => null,
		'total_itens' => 0,
		'per_page' => 100,
		'num_links' => 5,
		'uri_segment' => 'pag',
	);

	protected $tablesort = array(
		'pagination_url' => null,
		'uri_segment' => 'ordem',
		'sort_by' => 'id',
		'direction' => 'desc',
		'columns' => array(),
	);

	public function before()
	{
		parent::before();

		define('LANGUAGE', \Config::get('language'));

		/*
		 * Verifica se está logado
		 */
		$this->current_user = null;

		if (!Auth::check() && Request::active()->controller != 'Usuario\Controller_Controle_Login' && Request::active()->controller != 'Usuario\Controller_Controle_Recuperarsenha') {
			Response::redirect('controle/login');
		}
		if (Auth::check()) {
			$auth = Auth::instance();
			// Atribuir current_user para a instância para controladores usá-lo
			$this->current_user = \Usuario\Model_Usuario::find(Arr::get($auth->get_user_id(), 1));
			$this->checa_permissao();
		}
		/*
		 * Váriavel global.
		 */
		View::set_global('current_user', $this->current_user);

		/**
		 * Carregar JS, CSS e MENU, básicos do layout.
		 */
		$this->basico();
	}

	public function after($response)
	{
		/* Seta a URL Anterior */
		\Uri::set_back();

		/* Breadcrumb */
		\Breadcrumb_Controle::add_url_crumb(\Uri::main());

		/*
		 * Título da página <head><title></title></head>
		 */
		if (!isset($this->template->title)) {
			$this->template->title = strip_tags(\Config::get('site.geral.titulo'));
		}
		$this->template->title = $this->template->title;
		/*
		 * Se for Ajax, não usa o template
		 */
		if (\Input::is_ajax() === true and is_null($response)) {
			return \Response::forge($this->template->content);
		}

		return parent::after($response);
	}

	protected function basico()
	{
		/**
		 * CSS Files
		 */
		\Casset::css('controle::bootstrap.min.css');
		\Casset::css('googlefonts::Open+Sans:300italic,400italic,700italic,400,700,300');
		\Casset::css('controle::style.css');
		\Casset::css('controlelib::plupload/assets/jquery.plupload.queue.css');
		\Casset::css('controle::datepicker.css');
		\Casset::css('controle::colorpicker.css');
		\Casset::css('controle::codemirror.css');
		\Casset::css('controle::jquery-gmaps-latlon-picker.css');
		\Casset::css('controlelib::selectize/css/selectize.css');
		\Casset::css('controlelib::selectize/css/selectize.bootstrap3.css');

		if ($this->current_user) {
			/**
			 * JS Files
			 */
			\Casset::set_js_option('*', 'combine', false);
			\Casset::set_js_option('*', 'min', false);
			\Casset::js('controle::jquery-ui.js');
			\Casset::js('controle::bootstrap.min.js');
			\Casset::js('controlelib::ckeditor/ckeditor.js');
			\Casset::js('controlelib::ckeditor/styles.js');
			\Casset::js('controlelib::ckeditor/config.js');
			\Casset::js('controlelib::plupload/plupload.full.js');
			\Casset::js('controlelib::plupload/jquery.plupload.queue.js');
			\Casset::js('controlelib::plupload/lang/pt-br.js');
			\Casset::js('controle::jquery.friendurl.js');
			\Casset::js('controle::jquery.datepicker.js');
			\Casset::js('controle::jquery.colorpicker.js');
			\Casset::js('controle::jquery.maxlength.js');
			\Casset::js('controle::jquery.tablednd.js');
			\Casset::js('controle::jquery.clickpagination.js');
			\Casset::js('controle::bootstrap.maxlength.min.js');
			\Casset::js('controle::jquery.maskMoney.min.js');
			\Casset::js('controlelib::selectize/js/standalone/selectize.js');
			\Casset::js('controle::jquery-gmaps-latlon-picker.js');
			\Casset::js('controle::functions.js');

			$this->menu_controle();
		}
	}

	protected function menu_controle()
	{
		$menu = array(
			'navs' => array(
				'main' => array(
					'attr' => array('class' => 'nav navbar-nav'),
					'itens' => array(
						array(
							'label' => 'Painel',
							'link' => 'inicial',
						),
					),
				),
			),
		);

		if (\Auth::has_access('controle.contato')) {
			$menu['navs']['main']['itens'][] = array(
				'label' => 'Contatos',
				'link' => 'contato/listar',
				'permission' => 'contato.listar'
			);
		}

		if (\Auth::has_access('controle.configuracao')) {
			$menu['navs']['main']['itens'][] = array(
				'label' => 'Configurações',
				'link' => 'configuracao/editar/1',
				'permission' => 'configuracao.editar'
			);
		}

		if (\Auth::has_access('controle.usuario')) {
			$menu['navs']['main']['itens'][] = array(
				'label' => 'Usuários',
				'link' => 'usuario/listar',
				'permission' => 'usuario.listar'
			);
		}

		\Menu::forge('menu_controle', $menu, 'controle/');
	}

	protected function aplicar()
	{
		if ('POST' == \Input::method() and \Input::post('selected_itens')) {
			$registros = \Input::post('selected_itens');
			if (is_string($registros)) {
				$registros = explode(',', $registros);
				array_pop($registros);
			}
			$qtde_error = 0;
			switch (\Input::post('action')) {
				case 'excluir':
					foreach ($registros as $id) {
						if (!$this->excluir($id)) {
							$qtde_error++;
						}
					}
					break;

				case 'inativar':
					foreach ($registros as $id) {
						if (!$this->alterar_status($id, 0)) {
							$qtde_error++;
						}
					}
					break;

				case 'ativar':
					foreach ($registros as $id) {
						if (!$this->alterar_status($id, 1)) {
							$qtde_error++;
						}
					}
					break;
			}

			if ($qtde_error == 0) {
				\Message::success(__('message.aplicar.' . \Input::post('action') . '.sucesso'));
			} else {
				\Message::error(__('message.aplicar.' . \Input::post('action') . '.erro', array('qtde' => $qtde_error)));
			}
		}
		\Response::redirect_back('listar');
	}

	public function checa_permissao()
	{
		$has_access = true;

		if (!\Auth::has_access('controle.inicial')) {
			\Message::error(__('message.permissao.erro'));
			\Response::redirect_back('/');
		} else {
			$permissoes = \Request::active()->uri->segments();
			$qtde_permissao = count($permissoes);

			if ($qtde_permissao > 2) {
				if (($pos = array_search(\Request::active()->action, $permissoes)) !== false) {
					$permissao = trim($permissoes[$pos - 1] . '.' . \Request::active()->action);
				} else {
					$permissao = end($permissoes) . '.index';
				}
			} else {
				$permissao = trim(implode('.', $permissoes));
			}

			if ($permissao != 'controle' and $permissao != 'controle.inicial' and
				$permissao != 'controle.minhaconta' and $permissao != 'controle.logout' and
				!\Auth::has_access($permissao)) {
				Message::error(__('message.permissao.erro'));
				\Response::redirect_back('controle/inicial');
			}
		}

	}
}
