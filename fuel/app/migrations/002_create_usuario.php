<?php

namespace Fuel\Migrations;

class Create_usuario
{
    public function up()
    {
        \DBUtil::create_table('usuario', array(
            'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'user_id' => array('constraint' => 11, 'type' => 'int', 'default' => 0),
            'nome' => array('constraint' => 50, 'type' => 'varchar'),
            'username' => array('constraint' => 50, 'type' => 'varchar'),
            'email' => array('constraint' => 100, 'type' => 'varchar'),
            'password' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
            'group' => array('constraint' => 4, 'type' => 'tinyint'),
            'recuperar_senha_hash' => array('constraint' => 32, 'type' => 'varchar', 'null' => true),
            'last_login' => array('constraint' => 11, 'type' => 'int', 'null' => true),
            'previous_login' => array('constraint' => 25, 'type' => 'varchar', 'null' => true, 'default' => 0),
            'login_hash' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
            'profile_fields' => array('type' => 'text', 'null' => true),
            'status' => array('constraint' => 4, 'type' => 'tinyint', 'null' => true, 'default' => 0),
            'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
            'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

        ), array('id'));

        \DB::insert('usuario')->set(array(
            'id' => '1',
            'user_id' => '1',
            'nome' => 'Master',
            'username' => 'master',
            'password' => 'gmDpo3t+V3WnFmbqgsvDjJT1rBoqAQozJhK/zSkH+CA=',
            'email' => 'webmaster@revelare.com.br',
            'group' => '1',
            'last_login' => '1521579884',
            'previous_login' => '1442234903',
            'login_hash' => '49df13130040d839e8a0b7c42a87e5f47447bbf6',
            'status' => '1',
            'created_at' => '1371209865',
            'updated_at' => '1445083544',
        ))->execute();
    }

    public function down()
    {
        \DBUtil::drop_table('usuario');
    }
}
