<?php

namespace Fuel\Migrations;

class Create_configuracao
{
	public function up()
	{
		\DBUtil::create_table('configuracao', array(
            'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),

			'titulo' => array('constraint' => 150, 'type' => 'varchar', 'null' => true),
			'telefone' => array('constraint' => 15, 'type' => 'varchar', 'null' => true),
			'celular' => array('constraint' => 15, 'type' => 'varchar', 'null' => true),
			'email' => array('constraint' => 200, 'type' => 'varchar', 'null' => true),
			'endereco' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),

			'facebook' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'google_plus' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'instagram' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'linkedin' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'pinterest' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'twitter' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'youtube' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),

			'meta_title' => array('constraint' => 120, 'type' => 'varchar', 'null' => true),
			'meta_description' => array('constraint' => 155, 'type' => 'varchar', 'null' => true),
			'meta_keywords' => array('constraint' => 155, 'type' => 'varchar', 'null' => true),

			'conteudo_extra_css' => array('type' => 'longtext', 'null' => true),
			'conteudo_extra_texto' => array('type' => 'longtext', 'null' => true),

        ), array('id'));

        \DB::insert('configuracao')->set(array(
            'id' => 1,

			'titulo' => '',
			'telefone' => '',
			'celular' => '',
			'email' => '',
			'endereco' => '',

			'facebook' => 'https://pt-br.facebook.com/',
			'google_plus' => 'https://plus.google.com/',
			'instagram' => 'https://www.instagram.com/?hl=pt-br',
			'linkedin' => 'https://br.linkedin.com/',
			'pinterest' => 'https://br.pinterest.com/',
			'youtube' => 'https://twitter.com/?lang=pt-br',
			'twitter' => 'https://www.youtube.com/?gl=BR&hl=pt',

			'meta_title' => '',
			'meta_description' => '',
			'meta_keywords' => '',

			'conteudo_extra_css' => '',
			'conteudo_extra_texto' => '',
        ))->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('configuracao');
	}
}
