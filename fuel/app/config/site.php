<?php
return array(
	'titulo' => 'Santo Queijo',
	'geral' => array(),
	'recaptcha' => array(
			'sitekey' => '6LezTIYUAAAAAI48Caj-hSi5TjAx6EKwzmx8pPB_',
			'secretkey' => '6LezTIYUAAAAAABcnFoSt138rXf6sNeKRbxm1dQX',
	),

    'empresa' => 'revelare',
    'ssl' => false,
    'lang' => array(
        'pt_br' => 'Português',
        'en' => 'Inglês',
    ),
    'cliente_de' => array(
        'revelare' => array('titulo' => 'Revelare', 'telefones' => array('(14) 3879-6332', '/ 3204-0819'), 'logo' => 'controle::revelare.png'),
        'pao_criacao' => array('titulo' => 'Pão criação', 'telefones' => array('(14) 3313-9205'), 'logo' => 'controle::pao.png'),
    ),
);
