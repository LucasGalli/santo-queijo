<?php

return array(
    'menu' => array(
    	'home' => array(
			'titulo' => 'Home',
			'url_amigavel' => '/',
		),
        'sendi'  => array(
            'titulo' => 'A Sendi',
            'url_amigavel' => 'a-sendi',
        ),
        'fabricados'  => array(
            'titulo' => 'Pré-fabricados',
            'url_amigavel' => 'pre-fabricados',
        ),
        'engenharia'  => array(
            'titulo' => 'Engenharia',
            'url_amigavel' => 'engenharia',
        ),
        'obras'  => array(
            'titulo' => 'Obras',
            'url_amigavel' => 'obras',
        ),
        'responsabilidade'  => array(
            'titulo' => 'Responsabilidade',
            'url_amigavel' => 'responsabilidade',
        ),
        'noticias'  => array(
            'titulo' => 'Notícias',
            'url_amigavel' => 'noticias',
        ),
        'faleconosco'  => array(
            'titulo' => 'Fale Conosco',
            'url_amigavel' => 'contato',
        ),
        'orcamento' => array(
            'texto' => 'Solicite um orçamento',
            'url_amigavel' => 'contato',
        ),
    ),

    'entrar' => array(
        'botao' => '&nbsp;&nbsp;ENTRAR',
    ),

	'sair' => array(
		'botao' => '&nbsp;&nbsp;SAIR',
	),

	'area_restrita' => array(
		'botao' => '&nbsp;&nbsp;Área Restrita',
	),

    'login' => array(
    	'usuario' => 'Usuário',
    	'senha' => 'Senha',
		'botao' => 'Login'
	),

    'cards' => array(
        'produtos' => array(
            'btn_detalhes' => 'Mais detalhes',
            'label' => 'Adicionar ao carrinho'
        ),
        'categorias' => array(
            'btn_detalhes' => 'Mais detalhes',
            'label' => 'Adicionar ao carrinho'
        ),
        'noticias' => array(
            'btn_detalhes' => 'Mais detalhes',
            'label' => 'Adicionar ao carrinho'
        ),
        'destaques' => array(
            'btn_detalhes' => 'Mais detalhes',
            'label' => 'Adicionar ao carrinho'
        ),
    ),

    'selos' => array(
        'titulo'    => 'Selos',
        'descricao' => 'Certificado Internacional <br> de Qualidade'
    ),

    'politica_qualidade' => array(
        'titulo' => 'Política de Qualidade',
        'descricao' => '',
        'btn_visualize' => 'Visualize nossa Política de Qualidade',
        'modal' => ''
    ),

    'footer' => array(
        'contato' => array(
            'titulo' => 'Contato'
        ),
        'menu' => array(
            'titulo' => 'Navegação'
        ),
        'coopyright' => array(
            'texto' => 'Haenke tubos metálicos flexíveis '.date("Y").' | Todos os direitos reservados',
        ),
    ),

    'pages' => array(
        'home'  => array(
            'titulo' => 'Home',
            'url_amigavel' => '/',
        ),
        'produtos'  => array(
            'titulo' => 'Produtos',
            'url_amigavel' => 'produtos',
        ),
        'noticias'  => array(
            'titulo' => 'Notícias',
            'url_amigavel' => 'noticias',
        ),
        'contato'  => array(
            'titulo' => 'Contato',
            'url_amigavel' => 'contato',
        ),
        'atendimento'  => array(
            'titulo' => 'Atendimento',
			'url_amigavel' => 'contato/atendimento',
			'descricao' => 'Entre em contato conosco através do formulário abaixo e deixe suas dúvidas ou sugestões.',
        ),
        'trabalhe_conosco'  => array(
            'titulo' => 'Trabalhe Conosco',
            'url_amigavel' => 'contato/trabalhe-conosco',
        ),
        'representantes'  => array(
            'titulo' => 'Representantes',
            'url_amigavel' => 'contato/representantes',
        ),
        'checkout' => array(
            'titulo' => 'Carrinho de Orçamentos',
            'subtitulo' => 'Dados de Contato',
            'url_amigavel' => 'carrinho',
            'btn_solicitar'  => 'Solicitar orçamento',
            'texto' => 'Não existem produtos no carrinho'
        ),
        'finalizado'  => array(
            'titulo' => 'Finalizado',
            'subtitulo' => 'Seu pedido de orçamento foi enviado com sucesso!',
            'url_amigavel' => 'finalizado',
        ),
    ),

    'erros' => array(
        'produtos' => array(
            'sem_cadastro' => 'Não existem produtos cadastrados.'
        ),
        'noticias' => array(
            'sem_cadastro' => 'Não existem notícias cadastradas.'
        ),
        'categorias' => array(
            'sem_cadastro' => 'Não existem categorias cadastradas.'
        ),
        'destaques' => array(
            'sem_cadastro' => 'Não existem destaques cadastrados.'
        ),
    ),
);
