<?php
return array(
	'home' => array(
		'header' => array(
			'titulo' => 'Engenharia',
			'subtitulo' => 'Pioneirismo com reconhecimento <br><span>e visão de futuro</span>',
			'texto' => 'Inovação e comprometimento aplicados a projetos que resultam em soluções de excelência, desde a parte civil à montagens eletromecânicas de equipamentos de geração e distribuição de energia.'
		),
		'banner' => array(
			'botao' => array(
				'url_amigavel' => 'engenharia/sobre',
				'texto' => 'saiba mais sobre a sendi engenharia &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>'
			),
			'link' => ''
		),
		'obras' => array(
			'titulo' => 'Obras',
			'subtitulo' => '<span>Qualidade Sendi</span><br> sempre presente',
			'texto' => 'Conheça algumas das grandes obras que levam o padrão Sendi de qualidade para todo o Brasil.',
			'botao' => 'veja todas as obras de engenharia&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => 'engenharia/obras',
		),
		'orcamento' => array(
			'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
			'botao' => array(
				'texto' =>	'SOLICITE UM ORÇAMENTO',
				'url_amigavel' => 'contato',
			)
		),
	),
	'obras' => array(
		'header' => array(
			'titulo' => 'Obras',
			'subtitulo' => '<span>Levando desenvolvimento</span> para todo o Brasil',
			'subsubtitulo' => 'Conheça nossas soluções',
	),
		'orcamento' => array(
			'texto' => 'Conheça nossas soluções para projetos no segmento de geração e distribuição de energia.',
			'botao' => 'Fale com um especialista',
			'url_amigavel' => 'contato',
		),
		'filtro' => array(
			'filtrarpor' => 'Filtrar por',
			'filtrar' => 'BUSCAR ',
		)
	),
	'sobre' => array(
		'header' => array(
			'titulo' => 'Sendi Engenharia',
			'subtitulo' => 'Pioneirismo com reconhecimento <br><span>e visão de futuro</span>',
			'texto' => 'Atuando em montagens industriais desde 1995, a Sendi Engenharia se destaca com soluções completas e inovadoras para construção civil. Somos especialista em montagens eletromecânicas em subestações de energia de sistemas de distribuição e transmissão de média, alta e extra alta tensão, sendo pioneira no Brasil na construção',
		),
		'paragrafo' => array(
			'titulo' => 'Reconhecimento, respeito <br><span>e foco no futuro</span>',
			'texto1' => 'Em nosso portfólio, temos orgulho de ter executado os principais projetos do segmento com as mais conceituadas empresas transmissoras de energia do Sistema Interligado Nacional (SIN), sendo:',
			'texto2' => '51 Subestações de até 230 kV',
			'texto3' => '60 Subestações de até 345 kV a 750 kV.',
			'texto4' => 'Nossos engenheiros e especialistas trabalham constantemente em busca de soluções rápidas para cada desafio dentro das Normas Técnicas e da boa prática da engenharia, atendendo as mais altas expectativas dos nossos clientes.',
		),
		'numeros' => array(
			'secao' => 'Números',
			'titulo' => 'A Sendi Engenharia em <span>números</span>',
			'icon1' => 'LOREM IPSUM',
			'icon2' => 'LOREM IPSUM',
			'icon3' => 'LOREM IPSUM',
			'icon4' => 'LOREM IPSUM',
		),
	),
	'obrasinterno' => array(
		'titulo' => '<span>Obras</span> relacionadas</h3>',
		'botao' => 'conheça todas as obras da sendi pré &nbsp;&nbsp;&nbsp;&nbsp;',
		'link' => 'engenharia/obras',
		'banner' => array(
			'texto' => 'Conheça nossas soluções para projetos no segmento de geração e distribuição de energia.',
			'botao' => 'FALE COM UM ESPECIALISTA',
			'url_amigavel' => 'contato',
		)
	)
);
