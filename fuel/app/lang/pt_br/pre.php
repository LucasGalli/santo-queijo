<?php
return array(
	'home' => array(
		'header' => array(
			'titulo' => 'Pré-fabricados',
			'subtitulo' => 'Liberdade para <span>criar</span>',
			'texto' => 'Especialistas em soluções completas em pré-fabricados, atendemos os segmentos: comercial, industrial, logística, escolas e universidades, hipermercados e supermercados, além de projetos especiais.',
			'cta' => array(
				'texto' => 'saiba mais sobre a sendi pré &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
				'url_amigavel' => 'pre-fabricados/sobre'
			),
			'bndes' => array(
				'titulo' => '<span>Viabilize sua obra via BNDES.</span>',
				'subtitulo' => 'Fale com um especialista para mais informações.',
			)
		),
		'produtos' => array(
			'titulo' => 'Produtos',
			'subtitulo' => 'Produtos',
			'texto' => 'Escadas, pilares, vigas, lajes, painéis, telhas w, são alguns dos produtos especiais fabricados pela Sendi Pré que são utilizados nas mais diferentes obras, resultando em estruturas, fechamentos e coberturas de altíssima qualidade de acordo com todas as normas técnicas brasileiras.',
			'botao' => array(
				'texto' => 'conheça nossa linha de produtos &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
				'url_amigavel' => 'pre-fabricados/produtos'
			)
		),
		'obras' => array(
			'titulo' => 'Obras',
			'subtitulo' => '<span>Qualidade Sendi</span><br> sempre presente',
			'texto' => 'Conheça algumas das grandes obras que levam o padrão Sendi de qualidade para todo o Brasil.',
			'botao' => array(
				'texto' => 'veja todas as obras da sendi pré&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
				'url_amigavel' => 'pre-fabricados/obras'
			)
		),
		'cta_orca' => array(
			'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
			'botao' => array(
				'texto' => 'solicite um orçamento',
				'url_amigavel' => 'contato'
			)
		),
		'obras-interno' => array(
			'relacionadas' => array(
				'titulo' => '<span>Obras</span> relacionadas',
				'botao' => 'conheça todas as obras da sendi pré &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
				'url_amigavel' => 'pre-fabricados/obras'
			),
			'orcamento' => array(
				'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
				'botao' => array(
					'texto' => 'SOLICITE UM ORÇAMENTO',
					'url_amigavel' => 'contato'
				),
			),
			'bndes' => array(
				'titulo' => '<span>Viabilize sua obra via BNDES.</span>',
				'subtitulo' => 'Fale com um especialista para mais informações.',
			),
		),
		'fabricados-obras' => array(
			'header' => array(
				'secao' => 'Obras',
				'titulo' => '<span>Qualidade Sendi</span> sempre presente',
				'subtitulo' => 'Conheça algumas obras realizadas.',
			),
			'filtro' => array(
				'filtrarpor' => 'Filtrar por',
				'filtrar' => 'BUSCAR ',
			),
			'orcamento' => array(
				'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
				'botao' => array(
					'texto' => 'SOLICITE UM ORÇAMENTO',
					'url_amigavel' => 'contato'
				)
			)
		),
		'fabricados-produtos-interno' => array(
			'itens' => array(
				'pilar' => 'Pilar',
				'texto' => 'Estruturas em concreto armado de seções variáveis. Quando necessário, condutores para escoamento de águas pluviais e consolos para apoio de vigas podem ser implantados',
				'botao' => array(
					'texto' => 'conheça nossa linha de produtos &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i>',
					'url_amigavel' => 'obras/interno'
				)
			),
			'especificacoes' => array(
				'titulo' => 'Especificações <br><span>técnicas</span>',
				'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
				'botao' => array(
					'texto' => 'SOLICITE UM ORÇAMENTO',
					'url_amigavel' => 'contato'
				)
			),
			'produtos' => array(
				'secao' => 'PRODUTOS',
				'titulo' => 'Estruturas, fechamentos, coberturas e <span>muito mais</span>',
				'subtitulo' => 'Conheça nossos produtos.',
			),
			'bndes' => array(
				'titulo' => '<span>Viabilize sua obra via BNDES.</span>',
				'subtitulo' => 'Fale com um especialista para mais informações.',
			)
		)
	),
	'fabricados-produtos' => array(
		'top' => array(
			'secao' => 'Produtos Pré-fábricados',
			'titulo' => 'Estruturas, fechamentos, coberturas e <span>muito mais!</span>',
			'subtitulo' => 'Conheça nossos produtos',
		),
		'orcamento' => array(
			'texto' => 'Grandes obras levam as soluções em pré-fabricados da Sendi Pré para todo o Brasil, faça parte!',
			'botao' => array(
				'texto' => 'SOLICITE UM ORÇAMENTO',
				'url_amigavel' => 'contato'
			)
		),
		'bndes' => array(
			'titulo' => '<span>Viabilize sua obra via BNDES.</span>',
			'subtitulo' => 'Fale com um especialista para mais informações.',
		)
	),
	'fabricados-sobre' => array(
		'header' => array(
			'secao' => 'Pré-fábricados',
			'titulo' => 'Liberdade para <span>criar</span>',
			'subtitulo' => 'Estamos prontos para enfrentar qualquer desafio!',
			'texto-main' => 'Através do sistema construtivo de pré-fabricados e peças em concreto armado, nossos produtos atendem a todas as necessidades no segmento de construção civil, oferecendo melhor custo-benefício para o seu projeto, seja ele comercial, industrial, logística, escolas e universidades, hipermercados e supermercados, entre outros.',
		),
		'beneficios' => array(
			'secao' => 'Benefícios',
			'titulo' => 'Conheça os benefícios dos produtos <span>pré-fabricados Sendi Pré</span>',
			'lista' => array(
				'item1' => 'Otimizam os recursos disponíveis, visando a eliminação de desperdícios',
				'item2' => 'Segurança estrutural e alta resistência ao fogo',
				'item3' => 'Peças uniformes e montagem precisa',
				'item4' => 'Maior durabilidade',
				'item5' => 'Manutenção econômica',
				'item6' => 'Soluções personalizadas e exclusivas',
			)
		),
		'numeros' => array(
			'secao' => 'Números',
			'titulo' => 'A Sendi Pré em <span>números</span>',
			'icones' => array(
				'icon1' => 'LOREM IPSUM',
				'icon2' => 'LOREM IPSUM',
				'icon3' => 'LOREM IPSUM',
				'icon4' => 'LOREM IPSUM',
			)
		),
		'bndes' => array(
			'titulo' => '<span>Viabilize sua obra via BNDES.</span>',
			'subtitulo' => 'Fale com um especialista para mais informações.',
		)
	)
);

