<?php

return array(
	'leia_mais' => 'Leia mais +',

	'interno' => array(
		'leia_mais' => 'Leia mais +',
		'todas' => array(
			'texto' => 'VEJA TODAS AS NOTÍCIAS&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => '#',
			'share' => 'Compartilhe essa notícia',
		),
	'ultimas' => array(
		'titulo' => 'Últimas <span>notícias</span>',
	),
	),
	'header' => array(
		'titulo' => 'Notícias',
		'subtitulo' => 'Acompanhe as <span>novidades</span>',
		'subsubtitulo' => 'Fique por dentro dos principais acontecimentos da Sendi e do mercado, acompanhe.',
	),
	'pesquisa' => array(
		'buscarpor' => 'Buscar por:',
		'buscar' => 'BUSCAR ',
	),
);
