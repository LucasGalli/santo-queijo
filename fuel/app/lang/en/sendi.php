<?php

return array(
    'header' => array(
		'titulo'        => 'A Sendi',
		'subtitulo'     => 'Competência para <br><span>superar</span>',
		'texto'         => 'A SENDI iniciou suas atividades em 1995 no ramo de montagens industriais. Ao longo dos anos buscou oportunidades e desenvolveu novas áreas de atuação, tais como: construção civil e montagem eletromecânica em subestações de energia dos sistemas de distribuição e transmissão de média, alta e extra alta tensão, sendo pioneira no Brasil na construção e montagem de estação conversora de ultra alta tensão (800 kV cc).',
		'segundo-texto' => 'Após longas pesquisas tecnológicas, A SENDI expandiu ainda mais suas atividades com a divisão SENDI Pré, atuando na fabricação de peças pré-fabricadas em concreto armado, como: escadas, pilares, vigas, lajes, painéis, telhas w, através de projetos exclusivos para cada cliente. Consolidando-se assim no mercado de construção e montagem e atuando em todo o Brasil levando seus produtos para os diversos setores e obras com especialidade nos segmentos: comercial, industrial, logística, escolas e universidades, hipermercados e supermercados, além de projetos especiais.',
	),
	'linha-tempo' => array(
		'titulo'        => 'Linha do tempo',
		'subtitulo'     => 'Uma história de <br><span>sucesso</span>',
	),
	'missao-visao' => array(
		'titulo'        => 'Missão visão e política de qualidade',
		'subtitulo'     => 'A representação sólida do <span>nosso propósito</span>',

		'missao' => array(
			'titulo' => 'Missão',
			'texto' => 'Construir resultados fiéis às exigências dos clientes, com qualidade e respeito ao meio ambiente.'
		),
		'valores' => array(
			'titulo' => 'Valores',
			'linha-1' => 'Criatividade',
			'linha-2' => 'Tecnologia',
			'linha-3' => 'Velocidade',
			'linha-4' => 'Respeito ao meio ambiente',
			'linha-5' => 'Responsabilidade social'
		),
		'politica' => array(
			'titulo' => 'Política de qualidade',
			'linha-1' => '<strong>S</strong>atisfação dos clientes internos e externos',
			'linha-2' => '<strong>E</strong>volução e melhoria contínua dos processos',
			'linha-3' => '<strong>N</strong>ormatização e padronização',
			'linha-4' => '<strong>D</strong>esenvolvimento econômico e pessoal',
			'linha-5' => '<strong>I</strong>novação tecnológica'
		),
	),
	'atuacao' => array(
		'titulo' => 'Atuação',
		'subtitulo' => 'Atuando em todo o <span>território nacional</span>',
		'texto' => 'Atendendo os mais diversos perfis e exigências do mercado de engenharia e pré-fabricados, a SENDI se consolidou levando seus produtos e serviços para todo o território nacional com excelência em prazos e acompanhamento técnico dos processos.',
		'botao' => array(
			'texto' => 'Faça um orçamento &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => 'contato'
		)
	),
	'programa' => array(
		'titulo' => 'PROGRAMA 5S',
		'subtitulo' => 'Um plano estratégico <br><span>fundamental para a empresa</span>',
		'texto' => 'Conscientizar a todos da importância da qualidade e otimização dos nossos processos no ambiente de trabalho, com foco em resultados e no valor humano.',
		'icon1' => 'LOREM IPSUM',
		'icon2' => 'LOREM IPSUM',
		'icon3' => 'LOREM IPSUM',
	),
	'certificados' => array(
		'titulo' => 'Certificados',
		'subtitulo' => 'Qualidade <span>certificada e reconhecida</span>',
		'texto' => ' A divisão SENDI PRÉ é certificada ISO 9001:2015 e possui o Selo de Excelência ABCIC. Conquistas que reforçam o nosso compromisso de levar produtos e serviços de qualidade superior, reconhecidos através de:',
		'linha-1' => 'Satisfação total dos clientes internos e externo',
		'linha-2' => 'Evolução e melhoria contínua dos processos',
		'linha-3' => 'Normatização e padronização',
		'linha-4' => 'Desenvolvimento econômico e pessoal',
		'linha-5' => 'Inovação e soluções tecnológicas',
		'iso' => array(
			'titulo' => 'ISO 9001:2015',
			'texto' => 'Normatização de todos os processos na gestão da qualidade'
		),
		'abcic' => array(
			'titulo' => 'ABCIC',
			'texto' => 'Aplicação das melhores práticas no setor de pré-fabricados'
		),
	),
);
