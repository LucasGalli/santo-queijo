<?php
return array(
	'registro' => array(
		'nao_encontrado' => 'Não foi possível encontrar o registro.',
	),
	'salvar' => array(
		'sucesso' => 'O registro foi salvo com sucesso.',
		'erro' => 'Não foi possível salvar o registro. Tente novamente.',
	),
	'excluir' => array(
		'sucesso' => 'O registro foi excluído com sucesso.',
		'erro'    => 'Não foi possível excluir o registro. Tente novamente.',
	),
	'ativar' => array(
		'sucesso' => 'O registro foi ativado com sucesso.',
		'erro'    => 'Não foi possível ativar o registro. Tente novamente.',
	),
	'inativar' => array(
		'sucesso' => 'O registro foi inativado com sucesso.',
		'erro'    => 'Não foi possível inativar o registro. Tente novamente.',
	),
	'aplicar' => array(
		'excluir' => array(
			'sucesso' => 'Os registros foram excluídos com sucesso.',
			'erro'    => 'Não foi possível inativar <strong>:qtde</strong> registro(s).<br />Principais motivos: Você não tem permissão para exclui-lo(s) ou instabilidade na conexão.',
		),
		'ativar' => array(
			'sucesso' => 'Os registros foram ativados com sucesso.',
			'erro'    => 'Não foi possível inativar <strong>:qtde</strong> registro(s).<br />Principais motivos: Você não tem permissão para exclui-lo(s) ou instabilidade na conexão.',
		),
		'inativar' => array(
			'sucesso' => 'Os registros foram inativados com sucesso.',
			'erro'    => 'Não foi possível inativar <strong>:qtde</strong> registro(s).<br />Principais motivos: Você não tem permissão para exclui-lo(s) ou instabilidade na conexão.',
		),
	),
	'login' => array(
		'erro' => 'Usuário e/ou senha inválido(s)',
	),
	'permissao' => array(
		'erro' => 'Você não tem permissão para executar esta ação!<br />O responsável pelo sistema foi informado do ocorrido.',
	),
	'recuperarsenha' => array(
		'sucesso' => 'Parabéns. Você redefiniu sua senha.',
		'erro' => 'Não foi possível redefinir a senha. Tente novamente.',
		'nao_encontrado' => 'Nenhuma conta foi encontrada com esse endereço de e-mail',
	),
	'email' => array(
		'erro' => 'Não foi possível enviar o e-mail. Tente novamente.',
		'sucesso' => 'Sua mensagem foi enviada com sucesso! Em breve responderemos seu contato.',
	),
	'formulario' => array(
		'sucesso' => 'Parabéns. O formulário foi enviado com sucesso.',
		'erro'    => 'Desculpe. Não foi possível enviar o formulário. Tente novamente.',
		'captcha' => 'Os caracteres não correspondem à imagem. Tente novamente.'
	),
	'orcamento' => array(
		'sucesso' => 'Seu pedido de orçamento foi enviado com sucesso.',
		'erro'    => 'Desculpe. Não foi possível enviar o formulário. Tente novamente.',
	),
	'feedback' => array(
		'sucesso' => 'Obrigado! Seu feedback foi enviado com sucesso.',
		'erro'    => 'Desculpe. Não foi possível enviar o seu feedback.',
	),
	'cadastro' => array(
		'sucesso' => 'Parabéns! Seu cadastro foi realizado com sucesso.',
		'pendente' => 'Parabéns! Seu cadastro foi realizado com sucesso e está aguardando aprovação. Assim que seu cadastro for aprovado você receberá uma confirmação em seu e-mail.',
		'erro' => 'Desculpe. Não foi possível realizar seu cadastro.',
		'erro_email' => 'Desculpe. Este e-mail já está sendo usado.',
	),
	'minha_conta' => array(
		'sucesso' => 'Sua conta foi atualizada com sucesso.',
		'erro' => 'Desculpe. Não foi possível atualizar sua conta.',
	)
);
