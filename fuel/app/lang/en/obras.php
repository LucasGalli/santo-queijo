<?php

return array(
    'obras-main' => array(
        'header' => array(
            'secao' => 'Obras',
            'titulo' => '<span>Qualidade</span> sempre presente',
            'subtitulo' => 'Conheça algumas das obras realizadas pela Sendi e confira a qualidade de sempre!',
        ),
        'filtro' => array(
            'filtrar' => 'Filtrar por',
            'buscar' => 'BUSCAR ',
        ),
        'orcamento' => array(
            'texto' => 'Estamos por trás de grandes obras que levam desenvolvimento pra todo o Brasil. Conte com a Sendi e tenha certeza da qualidade.',
            'link' => 'SOLICITE UM ORÇAMENTO',
            'url_amigavel' => 'contato',
        ),
    ),
    'interno' => array(
        'relacionadas' => '<span>Obras</span> relacionadas',
        'link' => 'conheça todas as obras da sendi pré &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
        'url_amigavel' => 'obras',
    ),
    'orcamento' => array(
        'texto' => 'Conheça nossas soluções para projetos no segmento de geração e distribuição de energia.',
        'link' => 'FALE COM UM ESPECIALISTA',
        'url_amigavel' => 'contato',
    )
);
