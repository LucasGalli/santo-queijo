<?php
// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';


Autoloader::add_classes(array(
    // Add classes you want to override here
    // Example: 'View' => APPPATH.'classes/view.php',
    'Auth_Login_Ormauth' => APPPATH.'classes/override/ormauth.php',
    'Auth_Login_Simpleauth' => APPPATH.'classes/override/simpleauth.php',
    'Casset' => APPPATH.'classes/override/casset.php',
    'Date' => APPPATH.'classes/override/date.php',
    'Inflector' => APPPATH.'classes/override/inflector.php',
    'Log' => APPPATH.'classes/override/log.php',
    'Request' => APPPATH.'classes/override/request.php',
    'Uri' => APPPATH.'classes/override/uri.php',
    'Validation' => APPPATH.'classes/override/validation.php',
    'Security' => APPPATH.'classes/override/security.php',
));

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : Fuel::DEVELOPMENT);

// Initialize the framework with the config file.
Fuel::init('config.php');
