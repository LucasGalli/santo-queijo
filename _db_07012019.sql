-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Jan-2019 às 18:20
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `santo-queijo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracao`
--

CREATE TABLE `configuracao` (
  `id` int(11) UNSIGNED NOT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `pinterest` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `meta_title` varchar(120) DEFAULT NULL,
  `meta_description` varchar(155) DEFAULT NULL,
  `meta_keywords` varchar(155) DEFAULT NULL,
  `conteudo_extra_css` longtext,
  `conteudo_extra_texto` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `configuracao`
--

INSERT INTO `configuracao` (`id`, `titulo`, `telefone`, `celular`, `email`, `endereco`, `facebook`, `google_plus`, `instagram`, `linkedin`, `pinterest`, `twitter`, `youtube`, `meta_title`, `meta_description`, `meta_keywords`, `conteudo_extra_css`, `conteudo_extra_texto`) VALUES
(1, '', '', '', '', '', 'https://pt-br.facebook.com/', 'https://plus.google.com/', 'https://www.instagram.com/?hl=pt-br', 'https://br.linkedin.com/', 'https://br.pinterest.com/', 'https://www.youtube.com/?gl=BR&hl=pt', 'https://twitter.com/?lang=pt-br', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) UNSIGNED NOT NULL,
  `fk_departamento` int(11) UNSIGNED DEFAULT NULL,
  `fk_vaga` int(11) UNSIGNED DEFAULT NULL,
  `tipo` tinyint(4) DEFAULT '0',
  `nome` varchar(150) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `assunto` varchar(150) DEFAULT NULL,
  `mensagem` text,
  `arquivo` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migration`
--

CREATE TABLE `migration` (
  `type` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `migration` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `migration`
--

INSERT INTO `migration` (`type`, `name`, `migration`) VALUES
('app', 'default', '001_create_configuracao'),
('app', 'default', '002_create_usuario'),
('app', 'default', '003_create_contato');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `group` tinyint(4) NOT NULL,
  `recuperar_senha_hash` varchar(32) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `previous_login` varchar(25) DEFAULT '0',
  `login_hash` varchar(255) DEFAULT NULL,
  `profile_fields` text,
  `status` tinyint(4) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `user_id`, `nome`, `username`, `email`, `password`, `group`, `recuperar_senha_hash`, `last_login`, `previous_login`, `login_hash`, `profile_fields`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Master', 'master', 'webmaster@revelare.com.br', 'gmDpo3t+V3WnFmbqgsvDjJT1rBoqAQozJhK/zSkH+CA=', 1, NULL, 1521579884, '1442234903', '49df13130040d839e8a0b7c42a87e5f47447bbf6', NULL, 1, 1371209865, 1445083544);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuracao`
--
ALTER TABLE `configuracao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `configuracao`
--
ALTER TABLE `configuracao`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
