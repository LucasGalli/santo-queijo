/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
	config.stylesSet = 'siteturbo';
	config.language = 'pt-br';
	config.uiColor = '#ffffff';

	config.extraPlugins += (config.extraPlugins ? ',youtube' : 'youtube');
	config.extraPlugins += (config.extraPlugins ? ',iframe' : 'iframe');
	config.extraPlugins += (config.extraPlugins ? ',bootstrapButtons' : 'bootstrapButtons');
	config.extraPlugins += (config.extraPlugins ? ',pageLink' : 'pageLink');
	config.extraPlugins += (config.extraPlugins ? ',justify' : 'justify');
	config.extraPlugins += (config.extraPlugins ? ',wordcount' : 'wordcount');

	config.wordcount = {
		// Whether or not you want to show the Paragraphs Count
		showParagraphs: false,
		// Whether or not you want to show the Word Count
		showWordCount: true,
		// Whether or not you want to show the Char Count
		showCharCount: true,
		// Whether or not you want to count Spaces as Chars
		countSpacesAsChars: false,
		// Whether or not to include Html chars in the Char Count
		countHTML: false,
		// Maximum allowed Word Count, -1 is default for unlimited
		maxWordCount: -1,
		// Maximum allowed Char Count, -1 is default for unlimited
		maxCharCount: -1
	};

	config.toolbar = [
		{name: 'document', items: ['Source']},
		{name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
		{name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', '-', 'RemoveFormat']},
		{name: 'insert', items: ['Image', 'Flash', 'Table', 'Iframe', 'BootstrapButtons', 'Templates', 'Youtube']},
		'/',
		{name: 'styles', items: ['Styles']},
		{
			name: 'paragraph',
			items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
		},
		{name: 'links', items: ['Link', 'PageLink', 'Unlink']},
		{name: 'tools', items: ['Maximize']},
	];

	config.height = 200;
	config.toolbarLocation = 'bottom';
	config.fontSize_defaultLabel = '14px';
	config.pasteFromWordPromptCleanup = true;
	config.contentsLanguage = 'pt-br';
	config.font_defaultLabel = 'Arial';

	config.contentsCss = [BASE + 'assets/css/bootstrap.min.css', BASE + 'assets/css/font-awesome.min.css', BASE + 'assets/css/style.css', BASE + 'assets/css/editor.css'];
	config.allowedContent = true;
	config.ignoreEmptyParagraph = false;

	config.filebrowserBrowseUrl = BASE + 'assets/controle/lib/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = BASE + 'assets/controle/lib/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = BASE + 'assets/controle/lib/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = BASE + 'assets/controle/lib/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = BASE + 'assets/controle/lib/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = BASE + 'assets/controle/lib/kcfinder/upload.php?type=flash';
};

CKEDITOR.on('instanceReady', function (ev) {
	ev.editor.dataProcessor.htmlFilter.addRules({
		elements: {
			img: function (el) {
				el.addClass('img-responsive');
			},
		}
	});
});
