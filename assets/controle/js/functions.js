(function ($) {
    $.fn.datepicker.dates['pt-BR'] = {
        days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
        daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
        daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        today: "Hoje"
    };
}(jQuery));


function load_selected_itens(elementos) {
    var selected_itens = '';
    $(elementos).each(function () {
        if ($(this).is(':checked')) {
            selected_itens += $(this).val() + ',';
        }
    });
    $('.td_checked').val(selected_itens);
}

function btnFixed() {
    if (($(window).scrollTop() + $(window).height()) > $('.btn-save-container').offset().top) {
        $('.btn-save').addClass('btn-save-static');
        $('.btn-back').addClass('btn-save-static');
    } else {
        $('.btn-save').css('right', $('.content > .container').offset().left);
        $('.btn-save').removeClass('btn-save-static');
        $('.btn-back').css('left', $('.btn-save-container').offset().left);
        $('.btn-back').removeClass('btn-save-static');
    }
}

function sortable() {
    $(".sortable").sortable({
        handle: '.drag',
        axis: 'y',
        start: function (event, ui) {
            ui.item.startPos = ui.item.index() + 1;
        },
        stop: function (event, ui) {
            ui.item.newPos = ui.item.index() + 1;

            if (ui.item.newPos != ui.item.startPos) {
                $.post("ordenar", {
                    id: $(ui.item).data('id'),
                    atual_posicao: ui.item.startPos,
                    nova_posicao: ui.item.newPos
                });
            }
        },
    });

    $(".sortable-gallery").sortable({
        handle: '.drag',
        axis: 'y',
        start: function (event, ui) {
            ui.item.startPos = ui.item.index() + 1;
        },
        stop: function (event, ui) {
            ui.item.newPos = ui.item.index() + 1;

            if (ui.item.newPos != ui.item.startPos) {
                $.post("../ordenar_galeria", {
                    id: $(ui.item).data('id'),
                    atual_posicao: ui.item.startPos,
                    nova_posicao: ui.item.newPos
                });
            }
        },
    });
}

$(document).ready(function () {
    $('.show-tooltip').tooltip();
    $('[data-toggle="tooltip"]').tooltip();
    $('.dropdown-toggle').dropdown();
    $('input[maxlength],textarea[maxlength]').maxlength();
    $('.form-control-colorpicker').colorpicker();
    $('.my-colorpicker-control').colorpicker();
    $('.form-control[maxlength]').maxlength();

    $('.btn-salvar').click(function () {
        $('#form-principal').submit();
    });

    /**
     * Calendário
     */
    $('.input-group.date').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        keyboardNavigation: false,
        forceParse: true,
        autoclose: true,
        todayHighlight: true,
        beforeShowDay: function (date) {
            if (date.getMonth() == (new Date()).getMonth())
                switch (date.getDate()) {
                    case 4:
                        return {
                            tooltip: 'Example tooltip',
                            classes: 'active'
                        };
                    case 8:
                        return false;
                    case 12:
                        return "green";
                }
        }
    });

    $('.click-pagination').clickPagination({
        'contentTarget': '.listar',
    });

    $('.money').maskMoney({
        symbol: '', /* Símbolo da moeda */
        decimal: ',', /* Separador de decimais */
        thousands: '.', /* Separador de milhares */
    });

    $('.selectize').selectize();

    /**
     * URL AMIGÁVEL
     */
    if ($("#form_translations\\[pt_br\\]\\[titulo\\]").length) {
        if ($("#form_translations\\[pt_br\\]\\[url_amigavel\\]").length) {
            $("#form_translations\\[pt_br\\]\\[titulo\\]").stringToSlug({
                getPut: '#form_translations\\[pt_br\\]\\[url_amigavel\\]'
            });
        }
        if (!$("#form_translations\\[pt_br\\]\\[meta_title\\]").length) {
            $("#form_translations\\[pt_br\\]\\[meta_title\\]").val($("#form_translations\\[pt_br\\]\\[titulo\\]").val());
        } else {
            $("#form_translations\\[pt_br\\]\\[titulo\\]").on('keyup', function () {
                $("#form_translations\\[pt_br\\]\\[meta_title\\]").val($(this).val());
            });
        }
    }

    if ($("#form_translations\\[en\\]\\[titulo\\]").length) {
        if ($("#form_translations\\[en\\]\\[url_amigavel\\]").length) {
            $("#form_translations\\[en\\]\\[titulo\\]").stringToSlug({
                getPut: '#form_translations\\[en\\]\\[url_amigavel\\]'
            });
        }
        if (!$("#form_translations\\[en\\]\\[meta_title\\]").length) {
            $("#form_translations\\[en\\]\\[meta_title\\]").val($("#form_translations\\[en\\]\\[titulo\\]").val());
        } else {
            $("#form_translations\\[en\\]\\[titulo\\]").on('keyup', function () {
                $("#form_translations\\[en\\]\\[meta_title\\]").val($(this).val());
            });
        }
    }

    if ($("#form_translations\\[es\\]\\[titulo\\]").length) {
        if ($("#form_translations\\[es\\]\\[url_amigavel\\]").length) {
            $("#form_translations\\[es\\]\\[titulo\\]").stringToSlug({
                getPut: '#form_translations\\[es\\]\\[url_amigavel\\]'
            });
        }
        if (!$("#form_translations\\[es\\]\\[meta_title\\]").length) {
            $("#form_translations\\[es\\]\\[meta_title\\]").val($("#form_translations\\[es\\]\\[titulo\\]").val());
        } else {
            $("#form_translations\\[es\\]\\[titulo\\]").on('keyup', function () {
                $("#form_translations\\[es\\]\\[meta_title\\]").val($(this).val());
            });
        }
    }

	if ($("#form_titulo_menu").length) {
		if (!$(".meta_title").length) {
			$('.meta_title').val($("#form_titulo_menu").val());
		}
		else {
			$("#form_titulo_menu").on('keyup', function () {
				$('.meta_title').val($(this).val());
			});
		}
	}
	else if ($("#form_titulo").length) {
		if (!$(".meta_title").length) {
			$('.meta_title').val($("#form_titulo").val());
		}
		else {
			$("#form_titulo").on('keyup', function () {
				$('.meta_title').val($(this).val());
			});
		}
	}

    $(".url_amigavel_pt_br").focusout(function () {
        $(".url_amigavel_pt_br").stringToSlug({
            getPut: '.url_amigavel_pt_br'
        });
    });

	$(".url_amigavel_en").focusout(function () {
		$(".url_amigavel_en").stringToSlug({
			getPut: '.url_amigavel_en'
		});
	});

	$('.btn-save-form-principal').click(function () {
        $('.form-principal').submit();
    });

    $('.check_all').click(function () {
        if ($(this).is(':checked')) {
            $('.td_checkbox').attr('checked', true);
        }
        else {
            $('.td_checkbox').attr('checked', false);
        }
    });

    /**
     * Ordenação Drag'n'Drop
     */


    /**
     * Ordenação Drag'n'Drop
     */
    $(".radio_pagina_inicial").change(function () {
        var pagina = $(this).val();
        $.post("pagina_inicial", {menu: pagina});
    });

    /**
     * Linha "Clicável" da Tabela
     */
    $(".linked-row").each(function () {
        var link = $(this).data("href");
        var row = $(this);
        row.click(function (e) {
            if (!$(e.target).hasClass('not-linked') && !$(e.target).hasClass('td_checkbox')) {
                document.location.href = link;
            }
        });
    });

    /**
     * Botão Salvar
     */
    if ($('.btn-save-container').length > 0) {
        btnFixed();
        $(window).bind('scroll', function () {
            btnFixed();
        });
    }

    /*
    * Sortable
    */
    sortable();
});
